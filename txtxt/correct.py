"""Objects and functions for correcting text data"""

import os, re, collections, json, difflib
from typing import Optional, List
import string

here = os.path.abspath(os.path.dirname(__file__))

class spellcor:
    def __init__(self, token_pat: Optional[str] = r'\b[a-zA-Z]+\b', lower: Optional[bool] = False,
                 wordsfile: Optional[str] = None, freqfile: Optional[bool] = False,
                 delim: Optional[str] = ',', kb_prox_ranking: Optional[bool] = True):
        """
        A modified Nordvig spelling correction object that also takes into account keyboard
        distance while determining which corrections to recommend the original code for the Nordvig
        spelling correction code can be found here: http://norvig.com/spell-correct.html

        Args:
            token_pat (Optional[str]): corpus tokenization regex pattern
            lower (Optional[bool]): whether or not to convert corpus to lowercase and match only
                lowercase candidates
            wordsfile (Optional[str]): the location of a custom corpus or word frequency/probability
                file used for correction candidate selection
            freqfile (Optional[bool]): whether or not the words file provided is a frequency file
                containing words and their frequency/probability of match
            delim (Optional[str]): the delimiter used in the frequency file --- default is comma
            kb_prox_ranking (Optional[bool]): whether or not to use keyboard proximity in
                determining the correction candidate (i.e., was it intered by a person?)

        Returns:
            a class object that can be used to correct misspellings or provide a set of candidate
                words from the corpus

        """

        self.kb_prox_ranking = kb_prox_ranking
        self.lower = lower

        tpat = re.compile(token_pat)

        if not wordsfile:
            wordsfile = os.path.join(here, 'corpi/big.txt')

        with open(wordsfile) as f:
            if freqfile:
                self.model = collections.Counter({
                    li.spit(delim)[0].strip(): li.spit(delim)[1].strip() for li in f.readlines()
                })
            elif self.lower:
                self.model = collections.Counter(
                    [ti.lower() for ti in tpat.findall(f.read())]
                )
            else:
                self.model = collections.Counter(
                    [ti for ti in tpat.findall(f.read())]
                )
        if self.kb_prox_ranking:
            with open(os.path.join(here, 'models/qwerty.json'), 'r') as f:
                self.kbmodel = json.load(f)
            
    def _kbdist(self, word1: str, word2: str):
        """
        The keyboard distance calculation function
        """
        # TODO: somday add keyboard models other than qwerty
        diffs = []
        s = difflib.SequenceMatcher(None, word1, word2)
        for tag, i1, i2, j1, j2 in s.get_opcodes():
            if tag is 'delete':
                diffs.append(2.0)
            elif tag is 'insert':
                diffs.append(2.0)
            elif tag is 'replace':
                for c1, c2 in zip(word1[i1: i2], word2[j1: j2]):
                    diffs.append(
                        sum([(i - j)**2 for i, j in zip(self.kbmodel[c1], self.kbmodel[c2])])**0.5
                    )
            else:
                pass
        return sum(diffs)

    def _prob(self, word2corr: str): 
        "Probability of `word`."
        if self.kb_prox_ranking: # place new ranking functions in here
            def _prob_ret(wordcand):
                return self._kbdist(word2corr, wordcand)
        else:
            def _prob_ret(wordcand):
                return 1.0 / self.model[wordcand]
        return _prob_ret

    def correction(self, word): 
        "Most probable spelling correction for word."
        return min(self.candidates(word), key=self._prob(word))

    def candidates(self, word: str): 
        "Generate possible spelling corrections for word."
        return (
            self._known([word]) or self._known(
                self._edits1(word)
            ) or self._known(
                self._edits2(word)
            ) or [word]
        )

    def _known(self, words: List[str]): 
        "The subset of `words` that appear in the dictionary of WORDS."
        return set(w for w in words if w in self.model)

    def _edits1(self, word: str):
        "All edits that are one edit away from `word`."
        letters    = string.ascii_lowercase if self.lower else string.ascii_letters
        splits     = [(word[:i], word[i:])    for i in range(len(word) + 1)]
        deletes    = [L + R[1:]               for L, R in splits if R]
        transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R)>1]
        replaces   = [L + c + R[1:]           for L, R in splits if R for c in letters]
        inserts    = [L + c + R               for L, R in splits for c in letters]
        return set(deletes + transposes + replaces + inserts)

    def _edits2(self, word: str): 
        "All edits that are two edits away from `word`."
        return (e2 for e1 in self._edits1(word) for e2 in self._edits1(e1))
