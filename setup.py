from setuptools import setup, find_packages
from os import path


here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

with open(path.join(here, 'VERSION'), encoding='utf-8') as f:
    version = f.read().strip()

with open(path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    requirements = [i.strip() for i in f.readlines()]

setup(
    name='txtxt',
    version=version,
    description='Tools for manipulating and modeling text data',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/pro9er5/txtxt',
    author='Phillip Rogers',
    author_email='pro9er5@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    packages=find_packages(exclude=['txtxt']),
    install_requires=requirements
)